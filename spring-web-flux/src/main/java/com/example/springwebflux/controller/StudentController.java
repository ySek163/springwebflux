package com.example.springwebflux.controller;

import java.time.Duration;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springwebflux.entity.Student;

import reactor.core.publisher.Flux;

@RestController
@RequestMapping(path = "/students")
public class StudentController {

	@GetMapping(value = "/flux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<Student> getAsFlux() {
		return Flux.interval(Duration.ofMillis(1000))
				.map ( i -> students[i.intValue()])
				.take(students.length);
	}

	@GetMapping(value = "/list")
	public Student[] getAsString() throws InterruptedException {
		Thread.sleep(1000);
		return students;
	}

	Student[] students = {
		new Student(1, "MUIT 一郎"),
		new Student(2, "MUIT 二郎"),
		new Student(3, "MUIT 三郎"),
		new Student(4, "MUIT 四郎"),
		new Student(5, "MUIT 五郎"),
		new Student(6, "MUIT 六郎"),
		new Student(7, "MUIT 七郎"),
		new Student(8, "MUIT 八郎"),
		new Student(9, "MUIT 九郎"),
		new Student(10, "MUIT 十郎"),
	};
}
