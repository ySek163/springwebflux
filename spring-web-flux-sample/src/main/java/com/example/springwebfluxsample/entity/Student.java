package com.example.springwebfluxsample.entity;

public class Student {

	public int id;
	public String name;

	@Override
	public String toString() {
		return id + ":" + name;
	}
}
